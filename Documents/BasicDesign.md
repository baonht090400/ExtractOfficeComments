# Basic design
## System architecture
**Overall architecture**
Using 3-tier architure pattern:
```plantuml
@startuml
actor Users as user
rectangle Application {
    node Presentation   as ui
    node Business       as logic
    node "Data acces"   as data
}
storage "Local PC" as fs {
    database    "File database"    as db
    file        "Files"         as files
}

left to right direction
user    -->     ui      :interact
ui      -->     logic   :user actions
logic   -->     ui      :update
logic   -->     data    :" request "
data    -->     logic   :respond
data    -->     db      :CRUD
data    -->     files   :CRUD
@enduml
```
## Feature list
|No.|Name|Description|
|:-:|:---|:----------|
|1|Select files|Users can select file in local PC from the file explorer list view|
|2|Deselect file|Users can deselect files from the selected list view|
|3|Extract comments|Extract the comments in the selected files and save as a excel file|

## Data design
### Data structure
The data is storage in different format during the extracting process:
1. In input files (docx, pptx, xlsx), data is saved in Open XML format.
2. In internal program, data is stored in Json format.
3. In database, SQLite is used to store data. Data is stored in a string during the transition between app and database.
4. In output files, data is presented in table style in an excel file.
  
A "comment" includes following properties:
- Document name
- Page
- Section
- Content
- Author

### Dataflow diagram
```plantuml
@startuml
file "Input files" as "iFile" {
    collections "Open XML data" as inData
}

rectangle App {
    collections "Json data" as appData
}

database "SQLite" as "db" {
    collections "Database tables" as dbData
}

file "Output files" as "oFile" {
    collections "Tables" as outData
}

left to right direction
inData --> appData
appData <-le-> dbData: \tstring    \n\n
appData --> outData
@enduml
```

## Workflows
### Select file
```plantuml
@startuml
|Users|
start
:Browse files in [File explorer] view;
:Click [Add] button;
|App|
:Add selected files to [Selected file] view;
|Users|
stop
@enduml
```

### Deselect files
```plantuml
@startuml
|Users|
start
:Select files in [Selected file] view;
:Click [Remove] button;
|App|
:Remove selected files from [Selected file] view;
|Users|
stop
@enduml
```

### Extract comments
```plantuml
@startuml
|Users|
start
:Click [Extract comments] button;
|App|
if (Exist items in [Selected items] view) then (Yes)
    :Display [Save as] dialog;
    |Users|
    :Select a file name to save output;
    |App|
    :Perform "Extract input" process|
    :Perform "Generate output" process|
    :Show notify message;
else (No)
|App|
    :Show error message;
endif
|Users|
stop
@enduml
```

#### Etract input process
```plantuml
@startuml
|App|
start
:Read input files;
:Parse input infotmation into data structure;
|Database|
:Write data to database;
|App|
stop
@enduml
```

#### Generate output process
```plantuml
@startuml
|App|
start
:Query neccesary data form database;
|Database|
:Return queried data;
|App|
:Parse queried data into data structures;
:Write output file;
stop
@enduml
```