# Detail design
## Presentation module
### Module architecture
```plantuml
@startuml
actor Users as user
node "Presentation" as "ui" {
    rectangle "Main screen" as "ms" {
        agent "File explorer view"  as feview
        agent "Selected files view" as slview
        agent "Buttons"             as bt
    }
    agent "UI interface"        as itf
}
node Business as logic

left to right direction
user --> ms
ms <-le-> itf
itf <-le-> logic
feview <.. bt
slview <.. bt
@enduml
```

## Business module architecture
```plantuml
@startuml
node Presentation as ui
node "Business" as "logic" {
    agent "Logic interface"         as itf
    node "Mediator"                 as med
    agent "Extract open XML"        as ext
    agent "Generate comment list"   as gen
}
node "Data access" as data

left to right direction
ui <-le-> itf
itf <-le-> data
itf -le-> med
med --> ext
med --> gen

@enduml
```

## Data access module architecture
```plantuml
@startuml
node Business as logic
node "Data access" as "data" {
    agent "Data access interface"   as itf
    agent "Database access"         as dbac
    agent "Files access"            as fac
}
storage "Local PC" {
    database Database as db
    file files
}

left to right direction
logic <-le-> itf
itf --> dbac
itf --> fac
dbac --> db
fac --> files

@enduml
```
