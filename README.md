# ExtractOfficeComments
- Overview: Extract command from Word, Power point and Excel.
- [Requirements definition](/Documents/RequirementsDefinition.md)
- [Basic design](/Documents/BasicDesign.md)
- [Detail design](/Documents/DetailDesign.md)
  
# Prerequirement
- .NET framework 6.0.

# License
This project is free of charge for everyone under the MIT license ([refer license.md](/LICENSE))